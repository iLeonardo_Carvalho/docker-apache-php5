![Apache PHP](https://lh3.googleusercontent.com/NqhRJ9Q6Av-Y_bbJ6wj1LDnS1vRBEwBT-UO124fkn6hme8n0l2Srdy4Kk-7NlZDWiYYsmeb3sLtn7855nqH6tCoIlhWqWI0W5-azYpWieldCTg1cdpLxfSwoFnLNtm3OW3nX6_xNjGR69B9uNeIeIull5cNttgkffWqULXZHpMU2oxW10FDLgnTId5GQ1V7x-nwac6qJ6nfYtqzO0js--8Gnd58zo_ajcN-N4Owrq7WwTI3NIwcZoXNxtyoYLKbM2bPRFuDoZOwe5ubVx3S8gcfeSMqQ8rP44xuGS_9U0FUI0rCFi3myWsEbBEUiMnetLJt3th2cDnS_4vdaO3s9tkl6SH36_xpzJIror2XMW_EZjL__tJcZz2IES6AFFVv8r8AtDHF4hJTPeVCWRvXtKbvLEA9U9k9XK52yfgem_FtTNkMLyrkWllqVgh3GqPTbcvEUTSSxH0ulBfvaAvOrR3NB4b9msYQoRWlzleVz9YPPXvkYkC3B-cFXZeEu25CshhwNQN-HkRzTZPamj9o9DsUR_YVBrVMSrnjXOkJkhYw45XZ-5FNSFfOECZ1UTucXqbp80IuFQczrybg_ArOtdmYjjrDvaWVD7qoRdIwsL8Jzl-Yu9g=w340-h148-no)




Docker HTTPD/PHP5
===================

Este container contém o source do **Apache Web HTTPD** com **PHP5** e é compilado através do **make && make install** usando a base original do Desenvolvedor.

----------



[TOC]

### Requisitos

Foi utilizado a base [ileonardo/httpd](https://hub.docker.com/r/ileonardo/httpd/), acrescentando o módulo **PHP5** na versão **Release** pelo site [Oficial do PHP](http://php.net/downloads.php) no dia 23 de Julho de 2016.

> **Notas:**

> - [HTTPD](#HTTPD): v2.4.23
> - [PHP5](#PHP5): v5.6.31

> **Obs.:**

> - Baseado no SO Debian/Ubuntu
> - LATEST (últimas atualizações de acordo com o acesso)
> - Comentado e adaptado ao LSB (Não testado o systemd)

#### Estrutura

**SERVER ROOT**
```
/:.
|
\---usr
 	|
 	\---local
 		|
 		\---apache2
 			|
 			+\	...
```
**DOCUMENT ROOT**
```
/:.
|
\---var
 	|
 	\---www
 		|
 		\---html
 			|
 			|	index.php (<?php phpinfo(); ?>)
```
**DEFAULT LAYOUT**
```
Apache Config file 		:: 	/usr/local/apache2/conf/httpd.conf
Other Config files 		:: 	/usr/local/apache2/conf/extra/
SSL Config file 		:: 	/usr/local/apache2/conf/extra/httpd-ssl.conf
ErrorLog 				:: 	/var/log/httpd/error.log
AccessLog 				:: 	/var/log/httpd/access.log
cgi-bin 				:: 	/usr/local/apache2/cgi-bin
 							(enabled by default, but some of the bundled scripts are 644)
binaries (apachectl) 	:: 	/usr/local/apache2/bin/
start/stop 				:: 	/usr/local/apache2/bin/apachectl
 							(start|restart|graceful|graceful-stop|stop|configtest|-D FOREGROUND)

# PORTAS
80 						:: 	Apache Web default
443 					:: 	Apache Web SSL (Certificado Digital)
```
> **Obs.:** Verifique o arquivo [envvars](#envvars), foi baseado nestas variaveis mas ainda não está automático, futuralmente estas variaveis estará junto com **Apache Config file**.

----------


Comandos Básicos
-------------------

#### Uso

Utilize:

Para construir a partir do Código Fonte:

```
docker build -t ileonardo/httpd-php5 .
```

> **Obs:** O ponto final significa que o **Dockerfile** esta na pasta atual.

Ou se preferir baixar pelo [Docker Hub](https://hub.docker.com/explore/):

```
docker pull ileonardo/httpd-php5
```

Para Executar:

```
docker run -d -p 80:80 -p 443:443 ileonardo/httpd-php5
```

Mas se preferir apontar o **DOCUMENT ROOT** no **HOST** do que usar do **Container**, utilize:

```
docker run -dit --name apache-app --publish=80:80 -v "/home/aluno/public":/var/www/html/ ileonardo/httpd-php5
```

Ou usando **$PWD** (apontando local atual):

```
docker run -dit --name apache-app --publish=80:80 -v "$PWD":/var/www/html/ ileonardo/httpd-php5
```

Entrar no Container em daemon:

```
docker exec -it <ID_Container> /bin/bash
```

Sair sem encerrar o Container:

```
[CTRL] p + q
```

#### Essenciais

Ver todas as Imagens no **HOST**:

```
docker images
```

Ver todos os Containers no **HOST**:

```
docker ps -a
```

Remover uma Imagem no **HOST**:

```
docker rmi <nome_imagem>
```

Remover um Container no **HOST**:

```
docker rm <Nome_Container>
```

Remover *dangling images* (Imagens sem TAG, quer dizer quando rodou o **Dockerfile** que falhou ele cria uma imagem <none>)

```
docker rmi $(docker images -f "dangling=true" -q)
```

Remover o histórico dos comandos do Container no **HOST**:

```
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
```

Remover Todas às Imagens e Containers no **HOST**:

```
docker stop $(docker ps -a -q) && \
docker rm $(docker ps -a -q) && \
docker rmi $(docker images -q)
```

#### Manipulação de Dados

Copiar um arquivo do Container para o **HOST**:

```
# Unix
docker cp <ID_Container>:/caminho/no/container/arquivo /caminho/no/host OR . (Local do Terminal)

# Windows
docker cp <ID_Container>:/caminho/no/container/arquivo c:\caminho\no\host OR . (Local do Terminal)
```

Copiar um arquivo do **HOST** para o Container:

```
# Unix
docker cp /caminho/no/host/arquivo <ID_Container>:/caminho/no/container

# Windows
docker cp c:\caminho\no\host\arquivo <ID_Container>:/caminho/no/container
```

Obter informações do container (PATH, STATUS, IP):

```
docker inspect <ID_Container>
```

Docker Toolbox:

```
docker-machine ls
```

#### Monitoramento de Containers

Para ver as estatísticas de um Container específico no **HOST**:

```
docker stats <Nome_container>
```

Para ver as estatísticas de **todos** Containers no **HOST**:

```
docker stats `docker ps | tail -n+2 | awk '{ print $NF }'`
```


Direitos autorais e Licença
-------------

Este trabalho não foi modificado de seus Criadores (Link's de consulta abaixo), foi adaptado de acordo com a documentação do mesmo e dando os créditos contida neste repositório, a busca e a organização para futuras atualizações deve ser dado ao **contributors.txt** (BY).

Este trabalho foi escrito por Leonardo Cavalcante Carvalho e está licenciado com uma [Licença **Apache-2.0**](https://www.apache.org/licenses/LICENSE-2.0).

[^stackedit]: [StackEdit](https://stackedit.io/) is a full-featured, open-source Markdown editor based on PageDown, the Markdown library used by Stack Overflow and the other Stack Exchange sites.
[^docker]: A instalação foi utilizado no domínio do [Docker](https://get.docker.com/) que está contido o script auto installer.
[^httpd]: A instalação Source das dependências foram baseadas no site apontado pelo [apache](http://ftp.unicamp.br/pub/apache/).
[^PHP5]: Manual de instalação oficial do [php.net](http://php.net/manual/en/install.unix.apache2.php).
[^Mundo Docker]: Configuração e a descrição do comando documentada baseado nos tutoriais do [Mundo Docker](www.mundodocker.com.br).